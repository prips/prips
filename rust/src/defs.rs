//! Common definitions for the IP range output tool.
/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::net::AddrParseError;

use std::str::FromStr;

use anyhow::{anyhow, Error as AnyError};
use thiserror::Error;

/// An error that most likely occurred while parsing the IP address specifications.
#[allow(clippy::error_impl_error)]
#[derive(Debug, Error)]
pub enum Error {
    /// Invalid start address for a CIDR range.
    #[error("CIDR base address didn't start at subnet boundary")]
    CidrBadStart,

    /// Not an integer prefix length.
    #[error("The CIDR prefix length must be an integer")]
    CidrInvalidPrefixLength,

    /// No prefix length for a CIDR range.
    #[error("No CIDR prefix length specified")]
    CidrNoPrefixLength,

    /// Invalid offset for a CIDR range.
    #[error("CIDR offsets are between 0 and 32")]
    CidrPrefixTooLarge,

    /// Something went really, really wrong.
    #[error("prips internal error")]
    Internal(#[source] AnyError),

    /// Could not parse an IP address.
    #[error("Could not parse '{0}' as a valid IP address")]
    ParseAddress(String, #[source] AddrParseError),

    /// Invalid IP range specified.
    #[error("The start address must be smaller than the end address")]
    StartBeforeEnd,
}

/// An inclusive IPv4 address range.
#[derive(Debug)]
pub struct AddrRange {
    /// The first address in the range.
    pub start: u32,
    /// The last address in the range.
    pub end: u32,
}

/// The address formatting mode.
#[derive(Debug, Clone, Copy)]
pub enum AddrFormat {
    /// Dotted-quad.
    Dot,
    /// A single decimal number.
    Dec,
    /// A single hexadecimal number.
    Hex,
}

impl FromStr for AddrFormat {
    type Err = AnyError;
    fn from_str(value: &str) -> Result<Self, Self::Err> {
        match value {
            "dot" => Ok(Self::Dot),
            "dec" => Ok(Self::Dec),
            "hex" => Ok(Self::Hex),
            _ => Err(anyhow!("Unrecognized address format specified")),
        }
    }
}

/// Runtime configuration for the IP range output tool.
#[derive(Debug)]
pub struct Config {
    /// The delimiter between two successive IP addresses.
    pub delim: char,
    /// The address formatting mode.
    pub format: AddrFormat,
    /// The address range to process.
    pub range: AddrRange,
    /// The loop step.
    pub step: usize,
}
