//! Output a range of IP addresses.
/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use crate::defs::{AddrFormat, Config, Error};

use anyhow::anyhow;

#[allow(clippy::print_stdout)]
fn format_dot(addr: u32, delim: char) {
    print!(
        "{first}.{second}.{third}.{fourth}{delim}",
        first = addr >> 24_i32,
        second = (addr >> 16_i32) & 255_u32,
        third = (addr >> 8_i32) & 255_u32,
        fourth = addr & 255_u32,
    );
}

#[allow(clippy::print_stdout)]
fn format_dec(addr: u32, delim: char) {
    print!("{addr}{delim}");
}

#[allow(clippy::print_stdout)]
fn format_hex(addr: u32, delim: char) {
    print!("{addr:x}{delim}");
}

/// Output a range of IP addresses.
pub fn output_range(cfg: &Config) {
    let formatter = match cfg.format {
        AddrFormat::Dot => format_dot,
        AddrFormat::Dec => format_dec,
        AddrFormat::Hex => format_hex,
    };
    for addr in (cfg.range.start..=cfg.range.end).step_by(cfg.step) {
        formatter(addr, cfg.delim);
    }
}

/// Convert a range into CIDR form.
#[allow(clippy::print_stdout)]
pub fn cidrize(cfg: &Config) -> Result<(), Error> {
    let diff = cfg.range.start ^ cfg.range.end;
    let (base, prefixlen) = if diff == 0 {
        (cfg.range.start, 32_u32)
    } else {
        let offset = diff.ilog2();
        if offset == 31 {
            (0, 0)
        } else {
            let mask = u32::MAX
                .checked_shl(offset)
                .ok_or_else(|| Error::Internal(anyhow!("Could not compute a mask for {offset}")))?;
            let prefixlen = 31_u32.checked_sub(offset).ok_or_else(|| {
                Error::Internal(anyhow!(
                    "Got an offset larger than 31 for {start}..{end}",
                    start = cfg.range.start,
                    end = cfg.range.end,
                ))
            })?;
            (cfg.range.start & mask, prefixlen)
        }
    };
    format_dot(base, '/');
    println!("{prefixlen}");
    Ok(())
}
