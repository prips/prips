#![warn(missing_docs)]
//! Display a range of IP addresses.
//!
//! The `prips` tool takes either a start and end address or a CIDR
//! subnet/prefixlen specification and displays the IPv4 addresses
//! contained within the specified range.
/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::env;

use anyhow::{bail, Context, Result};
use clap::error::ErrorKind as ClapErrorKind;
use clap::Parser;
use clap_derive::Parser;

mod defs;
mod parse;
mod prips;

use defs::{AddrFormat, Config};

const VERSION: &str = env!("CARGO_PKG_VERSION");

#[derive(Debug, Parser)]
/// The top-level command-line arguments.
#[clap(version)]
struct Cli {
    /// Print the range in CIDR notation".
    #[clap(short)]
    cidr: bool,

    /// Set the delimiter to the character with the specified ASCII code,
    #[clap(short, default_value("10"))]
    delim: u8,

    /// Set the address display format (hex, dec, or dot),
    #[clap(short, default_value("dot"), allow_hyphen_values(true))]
    format: AddrFormat,

    /// Display information about the supported features.
    #[clap(long)]
    features: bool,

    /// Increment by the specified number of addresses instead of one.
    #[clap(short('i'), default_value("1"))]
    step: usize,

    /// The CIDR range or the address to print from.
    first: Option<String>,

    /// The address to print to, if not a CIDR range.
    second: Option<String>,
}

/// The program operation mode: list addresses or do something else.
#[derive(Debug)]
enum Mode {
    /// Display usage or version information, already done.
    Handled,

    /// List the addresses in the specified range.
    List(Config),

    /// Represent a range in CIDR form.
    Cidrize(Config),
}

#[allow(clippy::print_stdout)]
fn parse_args() -> Result<Mode> {
    let args = match Cli::try_parse() {
        Ok(args) => args,
        Err(err)
            if matches!(
                err.kind(),
                ClapErrorKind::DisplayHelp | ClapErrorKind::DisplayVersion
            ) =>
        {
            err.print()
                .context("Could not display the usage or version message")?;
            return Ok(Mode::Handled);
        }
        Err(err) if err.kind() == ClapErrorKind::DisplayHelpOnMissingArgumentOrSubcommand => {
            err.print()
                .context("Could not display the usage or version message")?;
            bail!("Invalid or missing command-line options");
        }
        Err(err) => return Err(err).context("Could not parse the command-line options"),
    };

    if args.features {
        println!("Features: prips={VERSION} prips-impl-rust={VERSION}");
    }
    if args.features {
        return Ok(Mode::Handled);
    }

    let range = match (args.first, args.second) {
        (None, _) => bail!("No addresses specified"),
        (Some(first), None) => parse::parse_cidr(&first)?,
        (Some(first), Some(second)) => parse::parse_range(&first, &second)?,
    };

    let delim =
        char::from_u32(u32::from(args.delim)).context("Invalid delimiter character code")?;

    let cfg = Config {
        delim,
        format: args.format,
        range,
        step: args.step,
    };
    if args.cidr {
        Ok(Mode::Cidrize(cfg))
    } else {
        Ok(Mode::List(cfg))
    }
}

fn main() -> Result<()> {
    let mode = parse_args().context("Could not parse the command-line arguments")?;
    match mode {
        Mode::Handled => (),
        Mode::List(cfg) => prips::output_range(&cfg),
        Mode::Cidrize(cfg) => prips::cidrize(&cfg).context("Could not compute a CIDR range")?,
    };
    Ok(())
}
