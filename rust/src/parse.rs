//! Parse an IP address or a CIDR specification.
/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#![allow(clippy::module_name_repetitions)]

use std::net::Ipv4Addr;
use std::str::FromStr;

use anyhow::anyhow;

use crate::defs::{AddrRange, Error};

/// Parse an IPv4 address into an unsigned 32-bit number.
pub fn parse_addr(saddr: &str) -> Result<u32, Error> {
    let start_addr =
        Ipv4Addr::from_str(saddr).map_err(|err| Error::ParseAddress(saddr.to_owned(), err))?;
    let octets = start_addr.octets();
    Ok((u32::from(octets[0]) << 24)
        | (u32::from(octets[1]) << 16)
        | (u32::from(octets[2]) << 8)
        | u32::from(octets[3]))
}

/// Parse an address/prefixlen specification into an address range.
pub fn parse_cidr(spec: &str) -> Result<AddrRange, Error> {
    let (saddr, sprefix_len) = spec.split_once('/').ok_or(Error::CidrNoPrefixLength)?;
    let start = parse_addr(saddr)?;

    #[allow(clippy::map_err_ignore)] // we really don't care, it's invalid
    let prefix_len: u32 = sprefix_len
        .parse()
        .map_err(|_| Error::CidrInvalidPrefixLength)?;
    let err_internal = || {
        Error::Internal(anyhow!(
            "Unexpected arithmetic operation error for {start}/{prefix_len}"
        ))
    };
    match prefix_len {
        0 => {
            if start == 0 {
                Ok(AddrRange {
                    start,
                    end: u32::MAX,
                })
            } else {
                Err(Error::CidrBadStart)
            }
        }
        value if value < 32 => {
            let offset = 1_u32
                .checked_shl(32_u32.checked_sub(value).ok_or_else(err_internal)?)
                .ok_or_else(err_internal)?;
            let mask = offset.checked_sub(1).ok_or_else(err_internal)?;
            if (start & mask) == 0 {
                Ok(AddrRange {
                    start,
                    end: start.checked_add(mask).ok_or_else(err_internal)?,
                })
            } else {
                Err(Error::CidrBadStart)
            }
        }
        32 => Ok(AddrRange { start, end: start }),
        _ => Err(Error::CidrPrefixTooLarge),
    }
}

/// Parse two IPv4 addresses into a range.
pub fn parse_range(first: &str, second: &str) -> Result<AddrRange, Error> {
    let start = parse_addr(first)?;
    let end = parse_addr(second)?;
    if start <= end {
        Ok(AddrRange { start, end })
    } else {
        Err(Error::StartBeforeEnd)
    }
}
