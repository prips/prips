#!/bin/sh
#
# Copyright (c) 2017  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

if [ -f 'tap-functions.sh' ]; then
	. tap-functions.sh
elif [ -f 't/tap-functions.sh' ]; then
	. t/tap-functions.sh
else
	echo 'Bail out! Could not find tap-functions.sh'
	exit 99
fi

[ -z "$PRIPS" ] && PRIPS='./prips'

plan_ 8

v=`$PRIPS -d33 17.1.0.40/29 2>/dev/null`
res="$?"
exp='17.1.0.40!17.1.0.41!17.1.0.42!17.1.0.43!17.1.0.44!17.1.0.45!17.1.0.46!17.1.0.47!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi

v=`$PRIPS -d33 -i2 17.1.0.40/29 2>/dev/null`
res="$?"
exp='17.1.0.40!17.1.0.42!17.1.0.44!17.1.0.46!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi

v=`$PRIPS -d33 -i3 17.1.0.40/29 2>/dev/null`
res="$?"
exp='17.1.0.40!17.1.0.43!17.1.0.46!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi

v=`$PRIPS -d33 -i5 77.0.1.43 77.0.1.60 2>/dev/null`
res="$?"
exp='77.0.1.43!77.0.1.48!77.0.1.53!77.0.1.58!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi
