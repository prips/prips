# Copyright (C) 2001-2003  Daniel Kelly
# Copyright (C) 2009, 2010, 2013, 2016, 2018  Peter Pentchev
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

PROG=		prips
MAN1=		${PROG}.1
MAN1GZ=		${MAN1}.gz

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/man/man
DOCSDIR?=	${PREFIX}/share/doc/${PROG}
EXAMPLESDIR?=	${DOCSDIR}/examples

CPPFLAGS_STD?=	-D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700
CPPFLAGS+=	${CPPFLAGS_STD}

CFLAGS_OPT?=	-O2
CFLAGS_DBG?=	-g
CFLAGS_WARN?=	-Wall -W -std=c99 -pedantic -Wbad-function-cast -Wcast-align \
		-Wcast-qual -Wchar-subscripts -Winline -Wmissing-prototypes \
		-Wnested-externs -Wpointer-arith -Wshadow \
		-Wstrict-prototypes -Wwrite-strings \
		-Wno-format-y2k -Winline -Wno-pointer-sign \
		-Wreturn-type -Wswitch -Wunused-parameter

CFLAGS?=	${CFLAGS_OPT} ${CFLAGS_DBG}
CFLAGS+=	${CFLAGS_WARN}

CC?=gcc
LIBS?=
MKDIR?=		mkdir -p
RM?=rm -f
GZIP?=		gzip -c9
FALSE?=		false
INSTALL?=	install

# Keep this uncommented if you are building prips on a POSIX-compatible
# system.  If you are not sure, chances are that you most probably are.
CFLAGS+=	-DHAVE_POSIX
# Keep this uncommented if your header files provide a definition for
# the POSIX-compatible 32-bit unsigned integer type
CFLAGS+=	-DHAVE_UINT32_T
# If you do NOT have the POSIX-compatible uint32_t type, please specify
# which of the integer types on your platform is 32-bits wide
#CFLAGS+=	-DINT32_T=int

BINOWN?=	root
BINGRP?=	root
BINMODE?=	755

MANOWN?=	${BINOWN}
MANGRP?=	${BINGRP}
MANMODE?=	644

SHAREOWN?=	${BINOWN}
SHAREGRP?=	${BINGRP}
SHAREMODE?=	644

STRIP?=		-s

INSTALL_PROGRAM?=	${INSTALL} ${STRIP} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=		${INSTALL} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

all: ${PROG} ${MAN1GZ}

${PROG}: main.o prips.o except.o
	$(CC) $(LDFLAGS) -o prips main.o prips.o except.o $(LIBS)

main.o: main.c prips.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c main.c

prips.o: prips.c prips.h
	$(CC) $(CPPFLAGS) $(CFLAGS) -c prips.c 

except.o: except.c except.h
	$(CC) $(CPPFLAGS) $(CFLAGS) -c except.c

${MAN1GZ}:	${MAN1}
	${GZIP} ${MAN1} > ${MAN1GZ} || (${RM} ${MAN1GZ}; ${FALSE})

clean:	
	$(RM) core *~ *.o ${PROG} ${MAN1GZ}

test:	all
	prove t

install:	all
	${MKDIR} ${DESTDIR}${BINDIR}
	${INSTALL_PROGRAM} ${PROG} ${DESTDIR}${BINDIR}/
	${MKDIR} ${DESTDIR}${MANDIR}1
	${INSTALL_DATA} ${MAN1GZ} ${DESTDIR}${MANDIR}1/
	${MKDIR} ${DESTDIR}${EXAMPLESDIR}/tests
	${INSTALL_DATA} t/*.t ${DESTDIR}${EXAMPLESDIR}/tests/

.PHONY:	all clean test install
